node 'default' {

 class { 'apache':
  default_vhost => false,
 }

 apache::vhost { 'vhost.example.com':
  port    => 80,
  docroot => '/var/www/vhost',
 }

 file { '/var/www/vhost/index.html':
  ensure => file,
  content => "Teste aula puppet",
  require => Class['apache'],
 }
 
 #### Instalando Docker
 #include 'docker'
 
 class { 'docker': }
 
 docker::run { 'nginx':
  image   => 'nginx:latest',
  ports   => ['8080:80'],
  require => Class['docker'],
 }
}